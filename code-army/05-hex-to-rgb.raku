#!/usr/bin/env raku

subset RGB-code of Str where {
    | / ^ '#' <xdigit> ** 3 $ /
    | / ^ '#' <xdigit> ** 6 $ /
}
sub hex-to-rgb ( RGB-code $hex is copy ) {
    $hex .= substr(1);
    my Int @decimals = $hex.chars == 3 ?? $hex.comb.map(* x 2)».parse-base(16)
                                       !! $hex.comb(2)».parse-base(16);
    say @decimals;
}

hex-to-rgb('#abc');
hex-to-rgb('#aabbcc');
