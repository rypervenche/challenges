#!/usr/bin/env raku

grammar URL {
    token TOP              { <.protocol> <.domain> <query-string> }
    token protocol         { \w+ '://' }
    token domain           { <first-valid-char> <valid-chars>* '.' <valid-chars> ** 2..* }
    token first-valid-char { <+valid-chars - [-]> }
    token valid-chars      { <[\w] - [_]> }
    token query-string     { [ '/' [ '?' <query> [ '&' <query> ]* ]? ]?  }
    token query            { <type> '=' <value> }
    token type             { <[ -_.~\\\w ]>+ }
    token value            { <[ -_.~\\\w ]>+ }
}

class URL::Actions {
    method TOP ($/) {
        make $<query-string>.made;
    }
    method query-string ($/) {
        my Str %queries;
        for $<query> -> $query {
            %queries{~$query<type>} = ~$query<value>;
        }
        make %queries;
    }
}


my $url = 'https://google.com/?query=Google&type=cool';

my $actions = URL::Actions.new;
my $output = URL.parse($url, :$actions) or die "$!";

say ~$output;
say $output.made;
