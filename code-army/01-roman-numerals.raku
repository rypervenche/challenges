#!/usr/bin/env raku

my @mappings =
    1000 => < M MM MMM >,
    100  => < C CC CCC CD D DC DCC DCCC CM >,
    10   => < X XX XXX XL L LX LXX LXXX XC >,
    1    => < I II III IV V VI VII VIII IX >;

subset Allowed-Numeral of Int where 1 ≤ * ≤ 3399;

sub num-to-roman ( Allowed-Numeral:D $number --> Bool:D ) {
    my $temp-number = $number;
    my Str $roman-numeral;

    for @mappings -> $level {
        my $digit = ($temp-number / $level.key).Int;
        $roman-numeral ~= $level.value[$digit - 1] if $digit;
        $temp-number %= $level.key;
    }

    say "$number: $roman-numeral";
}

for 1..3399 {
    num-to-roman($_);
}
