#!/usr/bin/env raku

my @bills = < 100 50 20 10 5 1 >;
my @coins = < 25 10 5 1 >;

my @denominations = |@bills, |@coins »÷=» 100;

subset NonnegativeIntRat of Numeric where { $_ ~~ Int|Rat and $_ > 0 };

sub get-denomination-count ( NonnegativeIntRat:D $amount is copy --> Bool:D ) {
    my BagHash $weights;
    for @denominations -> $unit is copy {
        until $amount < $unit {
            my $divisible = ($amount - $unit);
            $weights{$unit}++;
            $amount -= $unit;
        }
    }
    print-weights($weights);
}

sub print-weights ( BagHash:D $weights --> Bool:D ) {
    for $weights.sort(-*.key) {
        printf( "%i — \$%.2f\n", .value, .key);
    }
}

get-denomination-count( prompt "Enter amount: " );

=begin pod
Your goal is to create a program that will prompt a user for input and create a
group of currency denominations that most efficiently meets the user's input.
=end pod
