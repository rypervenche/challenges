#!/usr/bin/env raku

say (0,1,*+*...*)[^100]
say (0,1,*+*…*)[^Ⅽ]

And another late one. I actually beat you! @theangryepicbanana#8297 (sorry for the ping)

**Challenge**: Fibonacci Sequence
**Type**: Code Golf
**Language**: Raku
**Length of solution**: 23
**Link**: https://repl.it/@rypervenche/Fibonacci-sequence
**Note**: Replacing `...` with `…` and `100` with `Ⅽ` reduces the char length to 19 (still 23 bytes though)
